package tdd.training.bsk;

public class Game {

	private Frame [] framear;
	private int FirstBonusThrow;
	private int SecondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
	    
		framear = new Frame[10];
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		int i=0,flag=0;
		while(i!=10 && flag!=1)
		{
			if(framear[i]==null)
			{
				framear[i]=frame;
				flag=1;
				
			}
			i++;
			
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		return framear[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.FirstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.SecondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return this.FirstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return this.SecondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int totalScore=0;
		for(int i=0;i!=10;i++)
		{
			if(framear[i].getFirstThrow()==10)
			{
				if(i==9 || i==8)
                { 
			    	 totalScore=totalScore+this.getFirstBonusThrow()+this.getSecondBonusThrow();
			    	 
                }
				 else
				 {
				 
				framear[i].setBonus(framear[i+1].getFirstThrow());
				if(framear[i+1].getFirstThrow()==10)
				{
					framear[i].setBonus(framear[i+2].getFirstThrow());
					
				}
				framear[i].setBonus(framear[i+1].getSecondThrow());
				 }
				
			}else
			{
				if(framear[i].getFirstThrow()+framear[i].getSecondThrow()>=10)
				{
                 if(i==9)
                  { 
                	 totalScore=totalScore+this.getFirstBonusThrow();
                	
                  }
                  else
				    {
                	  
                	
                	framear[i].setBonus(framear[i+1].getFirstThrow());
				   }
				}	
			}
			
			
			totalScore=totalScore+framear[i].getScore();
			
		}
		
		
		
		return totalScore;	
	}

}
