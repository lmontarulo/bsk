package tdd.training.bsk;

public class Frame {

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	private int firstThrow;
	private int secondThrow;
	private int bonuspoint;
	
	
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		this.firstThrow=firstThrow;
		this.secondThrow=secondThrow;
		bonuspoint=0;
		
	}

	

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		// To be implemented
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		// To be implemented
		return this.secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonuspoint=this.bonuspoint+bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return this.bonuspoint;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {

		int first=getFirstThrow();
		int result;
		int second=getSecondThrow();
		
		if(isSpare())
		{
			result=first+second+this.getBonus();
		}else
		{ 
			
		     if(isStrike())
		         {
		    	 result=first+this.getBonus();
		    	 
		           }
			else
			{
			result=first+second;
		    }
		}
		
	
		return result;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {

	return	this.firstThrow == 10  ; 
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		
		return this.firstThrow+this.secondThrow==10 ;
	}



	

}
