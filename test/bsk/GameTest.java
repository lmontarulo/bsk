package bsk;


import static org.junit.Assert.*;



import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class GameTest {

	@Test
	public void testGame() throws BowlingException
	{
        int firstThrow = 0,secondThrow=0;
		Frame Frame = new Frame(firstThrow, secondThrow);
		Game Game= new Game();
		Game.addFrame(Frame);
	}
	
	@Test
	public void testGameindexframe() throws BowlingException
	{
		int index=5;
		Game Game= new Game();
		Frame Frame0 = new Frame(1,5);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(3,6);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,6);
		Game.addFrame(Frame9);
	
		assertEquals(Frame5,Game.getFrameAt(index));
	}
	
	@Test
	public void testGameCalculateScore() throws BowlingException
	{
		Game Game= new Game();
		Frame Frame0 = new Frame(1,5);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(3,6);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,6);
		Game.addFrame(Frame9);
	
		assertEquals(81,Game.calculateScore());
	}
	
	@Test
	public void testGameCalculateScoreTestBonus() throws BowlingException{
	
		Game Game= new Game();
		Frame Frame0 = new Frame(1,9);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(3,6);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,6);
		Game.addFrame(Frame9);
		
	 	assertEquals(88,Game.calculateScore());
	}
	
	@Test
	public void testGameCalculateStrikeTestBonus() throws BowlingException{
	
		Game Game= new Game();
		Frame Frame0 = new Frame(10,0);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(3,6);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,6);
		Game.addFrame(Frame9);
		
	 	assertEquals(94,Game.calculateScore());
	}
	
	@Test
	public void testGameCalculateStrikeandconsecutiveSpareTestBonus() throws BowlingException{
	
		Game Game= new Game();
		Frame Frame0 = new Frame(10,0);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(10,0);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,6);
		Game.addFrame(Frame9);
		
	 	assertEquals(112,Game.calculateScore());
	}
	
	@Test
	public void testGameCalculateconsecutiveSpareTestBonus() throws BowlingException{
	
		Game Game= new Game();
		Frame Frame0 = new Frame(8,2);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(5,5);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,6);
		Game.addFrame(Frame9);
		
	 	assertEquals(98,Game.calculateScore());
	}
	
	@Test
	public void testGameCalculatelastSpareTestBonus() throws BowlingException{
	
		Game Game= new Game();
		Frame Frame0 = new Frame(1,5);
		Game.addFrame(Frame0);
		Frame Frame1 = new Frame(3,6);
		Game.addFrame(Frame1);
		Frame Frame2 = new Frame(7,2);
		Game.addFrame(Frame2);
		Frame Frame3 = new Frame(3,6);
		Game.addFrame(Frame3);
		Frame Frame4 = new Frame(4,4);
		Game.addFrame(Frame4);
		Frame Frame5 = new Frame(5,3);
		Game.addFrame(Frame5);
		Frame Frame6 = new Frame(3,3);
		Game.addFrame(Frame6);
		Frame Frame7 = new Frame(4,5);
		Game.addFrame(Frame7);
		Frame Frame8 = new Frame(8,1);
		Game.addFrame(Frame8);
		Frame Frame9 = new Frame(2,8);
		Game.addFrame(Frame9);
		int bonus=7;
		Game.setFirstBonusThrow(bonus);
		
	 	assertEquals(90,Game.calculateScore());
	}
	
	 @Test
	public void testGamesetFirstBonusThrow() throws BowlingException{
		 Game Game= new Game();
		 int bonus=7;
		Game.setFirstBonusThrow(bonus);
	 	assertEquals(7,Game.getFirstBonusThrow());
	}
	
	 @Test
		public void testGameCalculatelastStrikeTestBonus() throws BowlingException{
		
			Game Game= new Game();
			Frame Frame0 = new Frame(1,5);
			Game.addFrame(Frame0);
			Frame Frame1 = new Frame(3,6);
			Game.addFrame(Frame1);
			Frame Frame2 = new Frame(7,2);
			Game.addFrame(Frame2);
			Frame Frame3 = new Frame(3,6);
			Game.addFrame(Frame3);
			Frame Frame4 = new Frame(4,4);
			Game.addFrame(Frame4);
			Frame Frame5 = new Frame(5,3);
			Game.addFrame(Frame5);
			Frame Frame6 = new Frame(3,3);
			Game.addFrame(Frame6);
			Frame Frame7 = new Frame(4,5);
			Game.addFrame(Frame7);
			Frame Frame8 = new Frame(8,1);
			Game.addFrame(Frame8);
			Frame Frame9 = new Frame(10,0);
			Game.addFrame(Frame9);
			int bonus=7,bonus2=2;
			Game.setFirstBonusThrow(bonus);
			Game.setSecondBonusThrow(bonus2);
			
		 	assertEquals(92,Game.calculateScore());
		}
	 
	  @Test
	  public void testGamesetSecondBonusThrow() throws BowlingException{
		 Game Game= new Game();
		 int bonus=2;
		Game.setSecondBonusThrow(bonus);
	 	assertEquals(2,Game.getSecondBonusThrow());
	}
	  
	  
	  @Test
		public void testGameCalculatelastStrikePerfectGameTest() throws BowlingException{
		
			Game Game= new Game();
			Frame Frame0 = new Frame(10,0);
			Game.addFrame(Frame0);
			Frame Frame1 = new Frame(10,0);
			Game.addFrame(Frame1);
			Frame Frame2 = new Frame(10,0);
			Game.addFrame(Frame2);
			Frame Frame3 = new Frame(10,0);
			Game.addFrame(Frame3);
			Frame Frame4 = new Frame(10,0);
			Game.addFrame(Frame4);
			Frame Frame5 = new Frame(10,0);
			Game.addFrame(Frame5);
			Frame Frame6 = new Frame(10,0);
			Game.addFrame(Frame6);
			Frame Frame7 = new Frame(10,0);
			Game.addFrame(Frame7);
			Frame Frame8 = new Frame(10,0);
			Game.addFrame(Frame8);
			Frame Frame9 = new Frame(10,0);
			Game.addFrame(Frame9);
			int bonus=10,bonus2=10;
			Game.setFirstBonusThrow(bonus);
			Game.setSecondBonusThrow(bonus2);
			
		 	assertEquals(300,Game.calculateScore());
		}

}
