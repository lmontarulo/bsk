package bsk;




import static org.junit.Assert.*;



import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testFrame() throws BowlingException{
		int firstThrow = 0,secondThrow=0;
		Frame Frame = new Frame(firstThrow, secondThrow);
		
	 
	}
	
	@Test
	public void testFrameGetScore() throws BowlingException{
		int firstThrow = 2,secondThrow=6;
		Frame Frame = new Frame(firstThrow, secondThrow);
		assertEquals(8,Frame.getScore());
	 
	}

	
	@Test
	public void testFrameGetBnous() throws BowlingException{
		int firstThrow = 2,secondThrow=8,firstThrow1 = 2,secondThrow1=6;
		Frame Frame = new Frame(firstThrow, secondThrow);
		Frame Frame1 = new Frame(firstThrow1, secondThrow1);
		Frame.setBonus(firstThrow1);
		assertEquals(2,Frame.getBonus());
	 
	}
	

	
	@Test
	public void testFrameGetScoreWithBonus() throws BowlingException{
		int firstThrow = 2,secondThrow=8,firstThrow1 = 2,secondThrow1=6;
		Frame Frame = new Frame(firstThrow, secondThrow);
		Frame Frame1 = new Frame(firstThrow1, secondThrow1);
		Frame.setBonus(firstThrow1);
		assertEquals(12,Frame.getScore());
	 
	}
	
	@Test
	public void testFrameIsSpare() throws BowlingException{
		int firstThrow = 2,secondThrow=8;
		Frame Frame = new Frame(firstThrow, secondThrow);
		assertEquals(true,Frame.isSpare());
	}
	 
	@Test
	public void testFrameIsStrike() throws BowlingException{
		int firstThrow = 10,secondThrow=0;
		Frame Frame = new Frame(firstThrow, secondThrow);
		assertEquals(true,Frame.isStrike());
	}
	
	@Test
	public void testFramegetScoreStrikeBonus() throws BowlingException{
		int firstThrow = 10,secondThrow=0,firstThrow1 = 3,secondThrow1=6;
		Frame Frame = new Frame(firstThrow, secondThrow);
		Frame Frame1 = new Frame(firstThrow1, secondThrow1);
		Frame.setBonus(firstThrow1);
		Frame.setBonus(secondThrow1);
		assertEquals(19,Frame.getScore());
	}
	
	@Test
	public void testFramegetScoreMultipleStrikeBonus() throws BowlingException{
		int firstThrow = 10,secondThrow=0,firstThrow1 = 10,secondThrow1=0, firstThrow2 = 7,secondThrow2=2;
		Frame Frame = new Frame(firstThrow, secondThrow);
		Frame Frame1 = new Frame(firstThrow1, secondThrow1);
		Frame Frame2 = new Frame(firstThrow2, secondThrow2);
		Frame.setBonus(firstThrow1);
		Frame.setBonus(secondThrow1);
		Frame.setBonus(firstThrow2);
		assertEquals(27,Frame.getScore());
	}
	

	}
	
	


